# Ricrypt-Server

Easy-to-use, fast and stable node http-server for messaging. Goal is a matrix-like protocol<br>
Work in progress, maybe

### First Steps

Download source-code, include config.json with server configuration like so:
```json
{
	"port": 7000,
	"tokenLength": 150,
	"saltString": "aaaaaahjkoiuzgtfdfjhgtresdcvbghjkiolpvq5g04h2881hfvxbqce95hnzhm6",
	"saltLength": 128
}
```

edit configuration and run it with
```bash
$node server.mjs
```

### Read Documentation

if you want to create your own server and own clients, please read the files in ./docs