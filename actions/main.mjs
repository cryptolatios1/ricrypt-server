import fs from "node:fs";

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function main(data, request, path, postdata) {
	return {
		data: fs.readFileSync("./actions/main.html"),
		status: 201,
		contentType: "text/html"
	};
};