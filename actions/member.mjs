import { memberRoomPerms, roomPerms } from "../utils.mjs";

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function createRoomMember(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members) || room.members[userId].pending || !(((room.members[userId].permissions & roomPerms.createInvites) === roomPerms.createInvites) || ((room.members[userId].permissions & roomPerms.administrator) === roomPerms.administrator))) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let memberId = String(path[2] ?? "");
	if(memberId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let member = global.users[memberId];
	if(typeof member !== "object") {
		return {data: '{"error":"invalid user"}', status: 404, contentType: "application/json"};
	};

	if(memberId in global.rooms[roomId].members) {
		return {data: '{"error":"user already in room"}', status: 409, contentType: "application/json"};
	};

	let roomMember = {
		user: memberId,
		permissions: memberRoomPerms,
		nickname: null,
		pending: true,
	};

	global.rooms[roomId].members[memberId] = roomMember;
	global.users[memberId].rooms[roomId] = roomId;

	return {
		data: JSON.stringify({
			user: roomMember.user,
			permissions: roomMember.permissions,
			nickname: roomMember.nickname,
			pending: roomMember.pending
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {[permissions: number, nickname: string|null]} postdata*/
export function editRoomMember(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members) || room.members[userId].pending) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let memberId = String(path[2] ?? "");
	if(memberId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let roomMember = global.rooms[roomId].members[inviteUserId];
	if(typeof roomMember !== "object") {
		return {data: '{"error":"user not in room"}', status: 404, contentType: "application/json"};
	};

	let warnings = [];
	if("permissions" in postdata && (room.members[userId].permissions & roomPerms.manageRoomPermissions) === roomPerms.manageRoomPermissions) {
		let permissions = Number(postdata.permissions ?? 0);
		if(isNaN(permissions)) {
			warnings.push("invalid permissions");
		} else if((room.members[userId].permissions & permissions) === permissions) {
			warnings.push("you dont have this permission");
		} else {
			roomMember.permissions = permissions;
		};
	};

	if("nickname" in postdata) {
		let nickname = postdata.nickname === null ? null : String(postdatadata.nickname);
		if(nickname !== null && nickname.length === 0) {
			warnings.push("invalid nickname");
		} else {
			roomMember.nickname = nickname;
		};
	};

	return {
		data: JSON.stringify({
			user: roomMember.user,
			permissions: roomMember.permissions,
			nickname: roomMember.nickname,
			pending: roomMember.pending
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function viewRoomMember(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members) || room.members[userId].pending) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let memberId = String(path[2] ?? "");
	if(memberId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let member = room.members[memberId];
	if(typeof member !== "object") {
		return {data: '{"error":"user not in room"}', status: 404, contentType: "application/json"};
	};

	return {
		data: JSON.stringify({
			user: member.user,
			permissions: member.permissions,
			nickname: member.nickname,
			pending: member.pending
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function deleteRoomMember(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let inviteUserId = String(path[2] ?? "");
	if(inviteUserId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let inviteUser = global.users[inviteUserId];
	if(typeof inviteUser !== "object") {
		return {data: '{"error":"user not found"}', status: 404, contentType: "application/json"};
	};

	let roomMember = room.members[inviteUserId];
	if(typeof roomMember !== "object") {
		return {data: '{"error":"user not in room"}', status: 404, contentType: "application/json"};
	};

	if(room.members[userId].pending && inviteUserId !== userId || ((room.members[userId].permissions & roomPerms.kickMember) !== roomPerms.kickMember)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	delete global.rooms[roomId].members[inviteUserId];
	delete global.users[inviteUserId].rooms[roomId];

	return {
		data: JSON.stringify({
			user: roomMember.user,
			permissions: roomMember.permissions,
			nickname: roomMember.nickname,
			pending: roomMember.pending
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function accept(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let roomMember = room.members[userId];
	if(!roomMember.pending) {
		return {data: '{"error":"already accepted"}', status: 409, contentType: "application/json"};
	};

	global.rooms[roomId].members[userId].pending = false;

	return {
		data: JSON.stringify({
			user: roomMember.user,
			permissions: roomMember.permissions,
			nickname: roomMember.nickname,
			pending: roomMember.pending
		}),
		status: 201,
		contentType: "application/json"
	};
};