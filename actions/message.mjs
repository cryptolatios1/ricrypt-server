import { roomPerms } from "../utils.mjs";

/**@type {number}*/
global.lastMessageId = 0;

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{content: string}} postdata*/
export function createMessage(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = global.userTokens[token];
	if(!userId) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(!roomId) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(!room || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let content = String(postdata.content ?? "");
	if(content.length === 0) {
		return {data: '{"error":"missing content"}', status: 400, contentType: "application/json"};
	};

	let messageId = String(global.lastMessageId++);

	let message = {
		id: messageId,
		author: userId,
		content: content,
		created: Date.now()
	};

	global.rooms[roomId].messages[messageId] = message;

	return {
		data: JSON.stringify({
			id: message.id,
			author: message.author,
			content: message.content,
			created: message.created
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{content: string}} postdata*/
export function editMessage(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let messageId = String(path[2] ?? "");
	if(messageId.length === 0) {
		return {data: '{"error":"missing message"}', status: 400, contentType: "application/json"};
	};

	let message = room.messages[messageId];
	if(typeof message !== "object" || message.author !== userId) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	if("content" in postdata) {
		let content = String(postdata.content ?? "");
		if(content.length !== 0 && content !== message.content) {
			message.content = content;
		};
	};

	return {
		data: JSON.stringify({
			id: message.id,
			author: message.author,
			content: message.content,
			created: message.created
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function viewMessage(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let messageId = String(path[2] ?? "");
	if(messageId.length === 0) {
		return {data: '{"error":"missing message"}', status: 400, contentType: "application/json"};
	};

	let message = room.messages[messageId];
	if(typeof message !== "object") {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	return {
		data: JSON.stringify({
			id: message.id,
			author: message.author,
			content: message.content,
			created: message.created
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function deleteMessage(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members)) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let messageId = String(path[2] ?? "");
	if(messageId.length === 0) {
		return {data: '{"error":"missing message"}', status: 400, contentType: "application/json"};
	};

	let message = room.messages[messageId];
	if(typeof message !== "object" || !(userId === message.author || ((room.members[userId].permissions & roomPerms.manageMessages) === roomPerms.manageMessages) || ((room.members[userId].permissions & roomPerms.administrator) === roomPerms.administrator))) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	delete room.messages[messageId];

	return {
		data: JSON.stringify({
			id: message.id,
			author: message.author,
			content: message.content,
			created: message.created
		}),
		status: 200,
		contentType: "application/json"
	};
};