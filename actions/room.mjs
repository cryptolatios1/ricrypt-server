import fs from "fs";
import { roomPerms, fullRoomPerms, getImageType } from "../utils.mjs";

/**@type {number}*/
global.lastRoomId = 0;

/**@type {Object<string, room>}*/
global.rooms = {};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{name: string, description: string, icon: string|number[]}} postdata*/
export function createRoom(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let name = String(postdata.name ?? "");
	if(name.length === 0) {
		return {data: '{"error":"missing name"}', status: 400, contentType: "application/json"};
	};

	let roomId = String(++global.lastRoomId);

	let description = String(postdata.description ?? "");
	if(description.length === 0 || description.length > 1000) description = null;

	/**@type {Buffer}*/
	let icon = typeof postdata.icon === "string" ? Buffer.from(postdata.icon, "hex") : (Array.isArray(postdata.icon) ? postdata.icon.map(value => Number(value)) : null);
	let type = null;
	let hash = null;
	if(icon) {
		if((type = getImageType(icon)) === null) {
			icon = null;
		} else {
			hash = crypto.createHash("md5").update(icon).digest("base64");
			fs.writeFileSync(`./files/${roomId}_${hash}.${type}`, icon);
		};
	};

	let room = {
		id: roomId,
		name: name,
		description: description,

		iconHash: hash,
		iconType: type,

		members: {[userId]: {user: userId, nick: null, permissions: fullRoomPerms, pending: false}},
		messages: {}
	};

	global.rooms[roomId] = room;
	global.users[userId].rooms[room.id] = room.id;

	return {
		data: JSON.stringify({
			id: room.id,
			name: room.name,
			description: room.description,

			iconHash: room.iconHash,
			iconType: room.iconType,

			members: [room.members[userId]],
			messages: []
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{name: string, description: string, icon: string|number[]}} postdata*/
export function editRoom(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members) || room.members[userId].pending || !(((room.members[userId].permissions & roomPerms.manageRoom) === roomPerms.manageRoom) || ((room.members[userId].permissions & roomPerms.administrator) === roomPerms.administrator))) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	let warnings = [];
	if("name" in postdata) {
		let name = String(postdata.name ?? "");
		if(name.length === 0) {
			warnings.push("missing name");
		} else if(name !== room.name) {
			room.name = name;
		};
	};

	if("description" in postdata) {
		let description = String(postdata.name ?? "");
		if(description.length === 0) {
			warnings.push("missing description");
		} else if(description !== room.description) {
			room.description = description;
		};
	};

	if("icon" in postdata) {
		let icon = typeof postdata.icon === "string" ? Buffer.from(postdata.icon, "hex") : (Array.isArray(postdata.icon) ? postdata.icon.map(value => Number(value)) : null);
		if(icon) {
			let type = null;
			let hash = null;
			if(!(type = getImageType(icon))) {
				warnings.push('invalid icon-type');
			} else {
				room.iconHash = hash = crypto.createHash("md5").update(icon).digest("base64");
				room.iconType = type;
				fs.writeFileSync(`./files/${roomId}/${hash}`, icon);
			};
		} else {
			room.iconHash = null;
			room.iconType = null;
		};
	};

	return {
		data: JSON.stringify({
			id: room.id,
			name: room.name,
			description: room.description,

			iconHash: room.iconHash,
			iconType: room.iconType,

			members: Object.values(room.members),
			messages: Object.values(room.messages),

			warnings: warnings.length ? warnings : undefined
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function viewRoom(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let viewRoomId = String(path[1] || "");
	if(viewRoomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let viewRoom = global.rooms[viewRoomId];
	if(typeof viewRoom !== "object" || !(userId in viewRoom.members)) {
		return {data: '{"error":"missing permissions"}', status: 402, contentType: "application/json"};
	};

	return {
		data: JSON.stringify({
			id: viewRoom.id,
			name: viewRoom.name,
			description: viewRoom.description,

			iconHash: viewRoom.iconHash,
			iconType: viewRoom.iconType,

			/* Todo split or remove from view */
			members: Object.values(viewRoom.members),
			messages: viewRoom.members[userId].pending ? [] : Object.values(viewRoom.messages)
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {""} postdata*/
export function deleteRoom(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 400, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object" || !(userId in room.members) || (((room.members[userId].permissions & roomPerms.manageRoom) !== roomPerms.manageRoom) && ((room.members[userId].permissions & roomPerms.administrator) !== roomPerms.administrator))) {
		return {data: '{"error":"missing permissions"}', status: 403, contentType: "application/json"};
	};

	delete global.rooms[roomId];
	for(let member of Object.values(room.members)) {
		delete global.users[member.user].rooms[roomId];
	};

	return {
		data: JSON.stringify({
			id: room.id,
			name: room.name,
			description: room.description,

			iconHash: room.iconHash,
			iconType: room.iconType,

			/* Todo split or remove from view */
			members: Object.values(room.members),
			messages: Object.values(room.messages)
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IcomingMessage} request @param {string[]} path @param {{}} postdata*/
export function icon(data, request, path, postdata) {
	let roomId = String(path[1] ?? "");
	if(roomId.length === 0) {
		return {data: '{"error":"missing room"}', status: 401, contentType: "application/json"};
	};

	let room = global.rooms[roomId];
	if(typeof room !== "object") {
		return {data: '{"error":"invalid room"}', status: 401, contentType: "application/json"};
	};

	return {
		data: fs.readFileSync(`./files/${room.id}_${room.iconHash}`),
		status: 200,
		contentType: `image/${room.iconType}`
	};
};