import { getImageType, randomString } from "../utils.mjs";
import crypto from "crypto";
import fs from "fs";

/**@type {number}*/
global.lastUserId = 0;

/**@type {Object<string, user>}*/
global.users = {};

/**@type {Object<string, string>}*/
global.userNames = {};

/**@type {Object<string, string>}*/
global.userTokens = {};

/**@type {number}*/
global.tokenLength = 128;

/**@type {string}*/
global.saltString = "fghpo13668idhdwnflfkfhghfgqoepghdnvnyvsjfdoghiuFZFJDBDIQWUFHOFSDUJÖFÜFÄFLDKDJQURBFNADHZTWET123f567305nd05J55NFJFGUWOGEPD931";

/**@type {number}*/
global.saltLength = 128;

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{name: string, password: string, icon: string|number[]}} postdata*/
export function createUser(data, request, path, postdata) {
	/**@type {string}*/
	let name = String(postdata.name ?? "");
	if(name.length === 0) return {data: '{"error":"missing name"}', status: 400, contentType: "application/json"};
	if(name in global.userNames) {
		return {data: '{"error":"name already in use"}', status: 400, contentType: "application/json"};;
	};

	/**@type {string}*/
	let password = String(postdata.password ?? "");
	if(password.length === 0) {
		return {data: '{"error":"missing password"}', status: 400, contentType: "application/json"};
	};

	/**@type {string}*/
	let userId = String(++lastUserId);
	let token;
	do {
		token = randomString(global.tokenLength);
	} while (token in global.userTokens);

	/**@type {Buffer}*/
	let icon = typeof postdata.icon === "string" ? Buffer.from(postdata.icon, "hex") : (Array.isArray(postdata.icon) ? postdata.icon.map(value => Number(value)) : null);
	let type = null;
	let hash = null;
	if(icon) {
		if((type = getImageType(icon)) === null) {
			icon = null;
		} else {
			hash = crypto.createHash("md5").update(icon).digest("base64");
			fs.writeFileSync(`./files/${userId}_${hash}`, icon);
		};
	};

	let user = {
		id: userId,
		name: name,

		iconHash: hash,
		iconType: type,

		deleted: false,

		password: crypto.scryptSync(password, global.saltString, global.saltLength).toString(),
		token: token,
		rooms: {}
	};

	global.users[user.id] = user;
	global.userNames[user.name] = user.id;
	global.userTokens[user.token] = user.id;

	return {
		data: JSON.stringify({
			id: user.id,
			name: user.name,

			iconHash: user.iconHash,
			iconType: user.iconType,

			deleted: false,

			token: user.token,
			rooms: []
		}),
		status: 201,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {{name: string, password: string, icon: string|number[]}} postdata*/
export function editUser(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let user = global.users[userId];
	if(typeof user !== "object") {
		return {data: '{"error":"invalid user"}', status: 500, contentType: "application/json"}; /* should not happen */
	};

	let changes = {};
	if("name" in postdata) {
		let name = String(postdata.name ?? "");
		if(name.length !== 0 && name !== user.name && !(name in global.userNames)) {
			changes["name"] = name;
		};
	};

	if("password" in postdata) {
		let password = String(postdata.password ?? "");
		if(password.length !== 0) {
			changes["password"] = password;
		};
	};

	if("icon" in postdata) {
		let icon = typeof postdata.icon === "string" ? Buffer.from(postdata.icon, "hex") : (Array.isArray(postdata.icon) ? postdata.icon.map(value => Number(value)) : null);
		let type = null;
		if(icon !== null) {
			if((type = getImageType(icon)) === null) {
				icon = null;
			} else {
				changes["icon"] = icon;
				changes["iconType"] = type;
			};
		};
	};

	if("name" in changes) {
		delete global.userNames[user.name];
		global.userNames[changes.name] = changes.name;
		user.name = changes.name;
	};

	if("password" in changes) {
		user.password = crypto.scryptSync(password, global.saltString, global.saltLength).toString();
	};

	if("icon" in changes) {
		if(changes.icon === null) {
			fs.rmSync(`files/${user.id}_${user.iconHash}`);
			user.iconHash = null;
			user.iconType = null;
		} else {
			let hash = crypto.createHash("md5").update(changes.icon).digest("base64");
			fs.writeFileSync(`./files/${userId}/${hash}`, changes.icon);
			user.iconHash = hash;
			user.iconType = changes.iconType;
		};
	};

	return {
		data: JSON.stringify({
			id: user.id,
			name: user.name,

			iconHash: user.iconHash,
			iconType: user.iconType,

			deleted: false,

			token: user.token,
			rooms: Object.keys(user.rooms),
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {string} postdata*/
export function viewUser(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let viewUserId = String(path[1] || "");
	if(viewUserId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let viewUser = global.users[viewUserId];
	if(typeof viewUser !== "object") {
		return {data: '{"error":"invalid user"}', status: 404, contentType: "application/json"};
	};

	return {
		data: JSON.stringify({
			id: viewUser.id,
			name: viewUser.name,

			iconHash: viewUser.iconHash,
			iconType: viewUser.iconType,

			deleted: user.deleted
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {string} postdata*/
export function viewUserByName(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let viewUserIdRaw = String(path[1] || "");
	if(viewUserId.length === 0) {
		return {data: '{"error":"missing user"}', status: 400, contentType: "application/json"};
	};

	let viewUserId = global.userNames[viewUserId];
	if(typeof viewUserId !== "string") {
		return {data: '{"error":"invalid user"}', status: 404, contentType: "application/json"};
	};

	let viewUser = global.users[viewUserId];

	return {
		data: JSON.stringify({
			id: viewUser.id,
			name: viewUser.name,

			iconHash: viewUser.iconHash,
			iconType: viewUser.iconType,

			deleted: user.deleted
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IncomingMessage} request @param {string[]} path @param {string} postdata*/
export function deleteUser(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let user = global.users[userId];
	if(typeof user !== "object") {
		return {data: '{"error":"invalid user"}', status: 500, contentType: "application/json"}; /* should not happen */
	};

	if(user.iconHash) fs.rmSync(`./files/${user.id}/${user.iconHash}.${user.iconType}`);
	delete global.userTokens[user.token];
	delete global.userNames[user.name];

	user.name = `Deleted User ${randomString(7)}${user.id % 10}`;
	user.iconHash = null;
	user.iconType = null;
	user.password = null;
	user.token = null;
	user.deleted = true;
	user.rooms = {};

	return {
		data: JSON.stringify({
			id: user.id,
			name: user.name,

			iconHash: user.iconHash,
			iconType: user.iconType,

			deleted: true
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IcomingMessage} request @param {string[]} path @param {{name: string, password: string}} postdata*/
export function loginUser(data, request, path, postdata) {
	let name = String(postdata.name ?? "");
	if(name.length === 0) {
		return {data: '{"error":"missing name"}', status: 401, contentType: "application/json"};
	};

	let password = String(postdata.password ?? "");
	if(password.length === 0) {
		return {data: '{"error":"missing password"}', status: 401, contentType: "application/json"};
	};

	let user = global.userNames[name] && global.users[global.userNames[name]];
	if(typeof user !== "object" || user.password !== crypto.scryptSync(password, global.saltString, global.saltLength).toString()) {
		return {data: `{"error":"invalid credentials"}`, status: 401, contentType: "application/json"};
	};

	return {
		data: JSON.stringify({
			id: user.id,
			name: user.name,

			iconHash: user.iconHash,
			iconType: user.iconType,

			deleted: user.deleted,

			token: user.token,
			rooms: Object.keys(user.rooms)
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IcomingMessage} request @param {string[]} path @param {""} postdata*/
export function meUser(data, request, path, postdata) {
	let token = String(request.headers.authorization ?? "");
	if(token.length === 0) {
		return {data: '{"error":"missing token"}', status: 401, contentType: "application/json"};
	};

	let userId = String(global.userTokens[token] ?? "");
	if(userId.length === 0) {
		return {data: '{"error":"invalid token"}', status: 401, contentType: "application/json"};
	};

	let user = global.users[userId];
	if(typeof user !== "object") {
		return {data: '{"error":"invalid user"}', status: 500, contentType: "application/json"}; /* should not happen */
	};

	return {
		data: JSON.stringify({
			id: user.id,
			name: user.name,

			iconHash: user.iconHash,
			iconType: user.iconType,

			deleted: user.deleted,

			token: user.token,
			rooms: Object.keys(user.rooms),
		}),
		status: 200,
		contentType: "application/json"
	};
};

/**@param {{}} data @param {import("http").IcomingMessage} request @param {string[]} path @param {""} postdata*/
export function avatar(data, request, path, postdata) {
	let userId = String(path[1] ?? 0);
	if(userId.length === 0) {
		return {data: '{"error":"missing user"}', status: 401, contentType: "application/json"};
	};

	let user = global.users[userId];
	if(typeof user !== "object") {
		return {data: '{"error":"invalid user"}', status: 401, contentType: "application/json"};
	};

	return {
		data: fs.readFileSync(`./files/${user.id}_${user.iconHash}`),
		status: 200,
		contentType: `image/${user.iconType}`
	};
};