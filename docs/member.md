# Member

Base-Scheme

```json
{
	"id": "string", /* User-ID */
	"nickname": "string", /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": "boolean" /* If the user is invited */
}
```

## `POST` /roomMember/\<room-id\>/\<user-id\>
Invites a new Member

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"nickname": null, /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": true /* If the user is invited */
}
```

## `PATCH` /roomMember/\<room-id\>/\<user-id\>
Edits a Membmer

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:
```json
{
	/* <optional> */
	"nickname": "string", /* Nickname */
	"permissions": "number" /* new bitfield */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"nickname": "string", /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": "boolean" /* If the user is invited */
}
```

## `GET` /roomMember/\<room-id\>/\<user-id\>
Views a Member

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"nickname": "string", /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": "boolean" /* If the user is invited */
}
```

## `DELETE` /roomMember/\<room-id\>/\<user-id\>
Removes a Member

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"nickname": "string", /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": "boolean" /* If the user is invited */
}
```

## `POST` /accept/\<room-id\>
Accepts a Invite

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"nickname": "string", /* Nickname */
	"permissions": "number", /* Bitfield of permissions */
	"pending": false /* If the user is invited */
}
```