# Message

Base-Scheme

```json
{
	/* [private] */
	"id": "number", /* Message-ID */
	"author": "number", /* User-ID */
	"content": "string", /* Message-Content */
	"created": "number" /* timestamp when the message was created */
}
```

## `POST` /message/\<room-id\>
Sends a new Message

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:
```json
{
	/* [required] */
	"content": "string" /* content of the message */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* Message-ID */
	"author": "string", /* User-ID */
	"content": "string", /* Message-Content */
	"created": "number" /* timestamp when the message was created */
}
```

## `PATCH` /message/\<room-id\>/\<message-id\>
Edits a Message

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:
```json
{
	/* <optional> */
	"content": "string" /* new Content */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* Message-ID */
	"author": "string", /* User-ID */
	"content": "string", /* Message-Content */
	"created": "number" /* timestamp when the message was created */
}
```

## `GET` /message/\<room-id\>/\<message-id\>
Views a Message

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* Message-ID */
	"author": "string", /* User-ID */
	"content": "string", /* Message-Content */
	"created": "number" /* timestamp when the message was created */
}
```

## `DELETE` /message/\<room-id\>/\<message-id\>
Deletes a Message

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* Message-ID */
	"author": "string", /* User-ID */
	"content": "string", /* Message-Content */
	"created": "number" /* timestamp when the message was created */
}
```