# Room

Base-Scheme:

```json
{
	/* [private] */
	"id": "string", /* Room-ID */
	"name": "string", /* Name */
	"description": "string",  /* Description */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"members": "roomMember[]", /* Array containing room members */
	"messages": "message[]" /* Array containing messages */
}
```


## `POST` /room
Creates a new room

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:

```json
{
	/* [required] */
	"name": "string", /* Name */

	/* <optional> */
	"description": "string", /* Description */
	"icon": "string|number[]" /* hex-string or Buffer-Array */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* Roon-ID */
	"name": "string", /* Name */
	"description": "string",  /* Description */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"members": "roomMember[]", /* Array containing room members */
	"messages": [] /* Array containing messages */
}
```


## `PATCH` /room/\<room-id\>

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:
```json
{
	/* <optional> */
	"name": "string", /* Name */
	"description": "string", /* Description */
	"icon": "string|number[]" /* hex-string or Buffer-Array */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* Room-ID */
	"name": "string", /* Name */
	"description": "string",  /* Description */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"members": "roomMember[]", /* Array containing room members */
	"messages": "message[]" /* Array containing messages */
}
```

## `GET` /room/\<room-id\>

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* Room-ID */
	"name": "string", /* Name */
	"description": "string",  /* Description */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"members": "roomMember[]", /* Array containing room members */
	"messages": "message[]" /* Array containing messages */
}
```

## `DELETE` /room/\<room-id\>

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "number", /* Room-ID */
	"name": "string", /* Name */
	"description": "string",  /* Description */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"members": "roomMember[]", /* Array containing room members */
	"messages": "message[]" /* Array containing messages */
}
```

## `GET` /icon/\<room-id\>
Get full user icon

### Output:
Image-Buffer