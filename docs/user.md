# User

Base-Scheme:

```json
{
	/* [public] */
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": "boolean", /* wether the user is deleted */

	/* [private] */
	"token": "string", /* token for actions */
	"rooms": "number[]", /* Array with room-ids the user is part of */

	/* [intern] */
	"password": "string", /* hashed and salted password */
}
```

## `POST` /user
Register a new user

### Input
JSON-Body:
```json
{
	/* [required] */
	"name": "string", /* Name */
	"password": "string", /* Password */

	/* <optional> */
	"icon": "string|number[]" /* hex-string or Buffer-Array for User-Icon */
}
```

### Output:
JSON-Body
```json
{
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": false, /* wether the user is deleted */

	"token": "string", /* token for actions */
	"rooms": "string[]" /* Array with room-ids the user is part of */
}
```

## `PATCH` /user
Update your own user

### Input:
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

JSON-Body:
```json
{
	/* <optional> */
	"name": "string", /* Name */
	"password": "string", /* Password */
	"icon": "string|number[]" /* hex-string or Buffer-Array for User-Icon */
}
```

### Output
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": false, /* wether the user is deleted */

	"token": "string", /* token for actions */
	"rooms": "string[]" /* Array with room-ids the user is part of */
}
```

## `GET` /user/\<user-id\>
View a user profile

### Input:
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body:
```json
{
	"id": "string", /* ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": "boolean" /* wether the user is deleted */
}
```

## `DELETE` /user
Delete your own account

### Input:
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output
JSON-Body
```json
{
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": true /* wether the user is deleted */
}
```

## `POST` /login
Login with username and password

### Input
JSON-Body:
```json
{
	/* [required] */
	"name": "string", /* Name */
	"password": "string" /* Password */
}
```

### Output:
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": false, /* wether the user is deleted */

	"token": "string", /* token for actions */
	"rooms": "string[]" /* Array with room-ids the user is part of */
}
```

## `GET` /me
Get all data from your own account

### Input
Header:

| 		Name		| 	Value 	|
| ----------------- | --------- |
| `authorization`	| `<token>`	|

### Output:
JSON-Body:
```json
{
	"id": "string", /* User-ID */
	"name": "string", /* Name */

	"iconHash": "string", /* md5-hash of the icon */
	"iconType": "string", /* extension of the icon, eg .png */

	"deleted": false, /* wether the user is deleted */

	"token": "string", /* token for actions */
	"rooms": "string[]" /* Array with room-ids the user is part of */
}
```

## `GET` /avatar/\<user-id\>
Get full user icon

### Output:
Image-Buffer