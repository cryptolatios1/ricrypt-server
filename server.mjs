import https from "http";
import fs from "fs";

global.port = 7000;

/* public ressources, methods and default config */
let events = {
	"main": {
		"GET": (await import("./actions/main.mjs")).main
	},

	"user" : {
		"POST": (await import("./actions/user.mjs")).createUser,
		"PATCH": (await import("./actions/user.mjs")).editUser,
		"GET": (await import("./actions/user.mjs")).viewUser,
		"DELETE": (await import("./actions/user.mjs")).deleteUser
	},

	"login": {
		"POST": (await import("./actions/user.mjs")).loginUser
	},

	"me": {
		"GET": (await import("./actions/user.mjs")).meUser
	},

	"avatar": {
		"GET": (await import("./actions/user.mjs")).avatar
	},

	"room": {
		"POST": (await import("./actions/room.mjs")).createRoom,
		"PATCH": (await import("./actions/room.mjs")).editRoom,
		"GET": (await import("./actions/room.mjs")).viewRoom,
		"DELETE": (await import("./actions/room.mjs")).deleteRoom
	},

	"icon": {
		"GET": (await import("./actions/room.mjs")).icon
	},

	"roomMember": {
		"POST": (await import("./actions/member.mjs")).createRoomMember, /* invite */
		"PATCH": (await import("./actions/member.mjs")).editRoomMember,
		"GET": (await import("./actions/member.mjs")).viewRoomMember,
		"DELETE": (await import("./actions/member.mjs")).deleteRoomMember /* kick */
	},

	"accept": {
		"PATCH": (await import("./actions/member.mjs")).accept
	},

	"message": {
		"POST": (await import("./actions/message.mjs")).createMessage,
		"PATCH": (await import("./actions/message.mjs")).editMessage,
		"GET": (await import("./actions/message.mjs")).viewMessage,
		"DELETE": (await import("./actions/message.mjs")).deleteMessage
	}
};

/* Overwrite default config from files */
let config = JSON.parse(fs.readFileSync("config.json", "utf8") || "{}");
if("port" in config) global.port = Number(config.port);
if("tokenLength" in config) global.tokenLength = Number(config.tokenLength);
if("saltString" in config) global.saltString = String(config.saltString);
if("saltLength" in config) global.saltLength = Number(config.saltLength);

/* load database */
load();

/* create http server and listen to port */
https.createServer({}, async(request, response) => {
	/* get well-formed URL and ressource */
	let [url, param] = request.url.split("?", 2);
	let [_, ...path] = url.split(/\/+/);

	process.stdout.write(`${request.method}: ${url}\n`);

	/* if ressource is unknown */
	let event = events[path[0]];
	if(typeof event !== "object") {
		response.statusCode = 404;
		response.setHeader('content-type', 'application/json');
		response.end('{"error":"unknown url"}');
		return;
	};

	/* ressource known, maybe the method not */
	let event2 = event[request.method];
	if(typeof event2 !== "function") {
		response.statusCode = 405;
		response.setHeader('content-type', 'application/json');
		response.end('{"error":"method not allowed"}');
		return;
	};

	/* in case of POST or PATCH: fetch body (json) */
	let postdata = "";
	if(request.method === "POST" || request.method === "PATCH" || request.method === "PUT") {
		for await(let body of request) postdata += body;
		try {
			postdata = JSON.parse(postdata);
		} catch {
			response.statusCode = 400;
			response.setHeader('content-type', 'application/json');
			response.end('{"error":"invalid json"}');
			return;
		};
	};

	/* fetch normal URL-parameters */
	let data = {};
	if(param !== undefined) {
		/**@type {string{}}*/
		let parameterArray = param.split("&");
		let parameterArrayLength = parameterArray.length;
		for(let index = 0; index < parameterArrayLength; index++) {
			let pair = parameterArray[index].split("=");
			data[pair[0].trimEnd()] = pair[1].trimStart();
		};
	};

	/* return data from function, set content-type and status */
	let result = event2(data, request, path, postdata);
	response.statusCode = result.status;
	response.setHeader("content-type", result.contentType);
	response.end(result.data);
}).listen(port);

process.on("exit", code => {
	if(code !== -1) save();
}).on("SIGINT", () => {
	save();
	process.exit(-1);
});

function load() {
	if(!fs.existsSync("db.json")) return;

	let dbcontent = JSON.parse(fs.readFileSync("db.json", "utf8"));
	global.users = dbcontent.users ?? {};
	global.lastUserId = dbcontent.lastUserId ?? 0;

	global.rooms = dbcontent.rooms ?? {};
	global.lastRoomId = dbcontent.lastRoomId ?? 0;

	/* index user.name, user.token */
	for(let user of Object.values(global.users)) {
		if(user.deleted) continue;
		global.userNames[user.name] = user.id;
		global.userTokens[user.token] = user.id;
	};

	global.lastMessageId = dbcontent.lastMessageId ?? 0;
};

function save() {
	fs.writeFileSync("db.json", JSON.stringify({
		users: global.users, lastUserId: global.lastUserId,
		rooms: global.rooms, lastRoomId: global.lastRoomId,
		lastMessageId: global.lastMessageId
	}, undefined, "\t"));
};