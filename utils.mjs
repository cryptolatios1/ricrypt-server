import crypto from "crypto";

/* take native crypto.randomInt method and if not implemented Math.random() */
export let randomInt = crypto.randomInt ?? ((min, max) => Math.floor(Math.random()*(max === undefined ? min : (max-min)+min)));

/* create random strings */
export let randomString = (length, symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789") => {
	let newstring = "";
	for(let index = 0; index < length; index++) newstring += symbols[randomInt(symbols.length)];
	return newstring;
};

/* get magic number of file to determine file-type from buffer */
export let getImageType = (data) => {
	/* JPG: FF, D8, FF, DB */
	if(data[0] === 255 && data[1] === 216 && data[2] === 255 && data[3] === 219) return "jpg";

	/* JPEG: FF, D8, FF, EE */
	if(data[0] === 255 && data[1] === 216 && data[2] === 255 && data[3] === 238) return "jpeg";

	/* JPG: FF, D8, FF, E0 */
	if(data[0] === 255 && data[1] === 216 && data[2] === 255 && data[3] === 224) return "jpg";

	/* PNG: 89, 50, 4E, 47, 0D, 0A, 1A, 0A */
	if(data[0] === 137 && data[1] === 80 && data[2] === 78 && data[3] === 71 && data[4] === 13 && data[5] === 10 && data[6] === 26 && data[7] === 10) return "png";

	/* GIF: 47, 49, 46, 38, 37, 61 */
	if(data[0] === 71 && data[1] === 73 && data[2] === 70 && data[3] === 56 && data[4] === 55 && data[5] === 97) return "gif";

	/* GIF: 47, 49, 46, 38, 39, 61 */
	if(data[0] === 71 && data[1] === 73 && data[2] === 70 && data[3] === 56 && data[4] === 57 && data[5] === 97) return "gif";

	/* WEBP: 52, 49, 46, 46, ??, ??, ??, ??, 57, 45, 42, 50 */
	if(data[0] === 82 && data[1] === 73 && data[2] === 70 && data[3] === 70 && data[8] === 87 && data[9] === 69 && data[10] === 66 && data[11] === 80) return "webp";

	return null;
};

/* existing permissions for room-members */
export const roomPerms = {
	"createInvites": 1 << 0,
	"manageInvites": 1 << 1, /* view, delete */

	"manageMessages": 1 << 2,
	"kickMember": 1 << 5,

	"manageRoom": 1 < 10,
	"manageRoomPermissions": 1 << 11, /* view, delete, add */

	"administrator": 1 << 15
};

/* new members are only allowed to invite new users */
export const memberRoomPerms = roomPerms.createInvites;

/* permissions every room-administrator has */
export const fullRoomPerms = Object.keys(roomPerms).reduce((prev, current) => roomPerms[current] | prev, 0);